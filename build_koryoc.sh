#!/bin/bash

set -e

# Le script doit être executé en tant que root
if [ "$EUID" -ne 0 ]
  then echo "Vous devez etre root"
  exit
fi

# Ajout possibilité pour plus tard de changer l'architecture
ARCH="x86_64"

# Déclaration des variables de construction
BASEDIR=${PWD}
BUILD=/mnt/koryoc
SOURCEDIR=${BUILD}/sources
DESTDIR=${BUILD}/rootfs
ISODIR=${BUILD}/iso

# Déclaration des logiciels utilisés
KERNEL="4.14.39"
BUSYBOX="1.28.3"
MUSL="1.1.19"
SYSLINUX="6.03"

# Nettoyage puis création des principaux répertoires de construction
prepare_source()
{
    rm -Rf ${BUILD}
    mkdir ${BUILD} ${SOURCEDIR} ${DESTDIR} ${ISODIR}
}

# Construction des répertoires de notre système de base (FHS)
construction_arborescence()
{
    mkdir -p ${DESTDIR}/{bin,boot,dev,etc,home,lib,media,mnt,proc,root,sys,tmp,var}
    mkdir -p ${DESTDIR}/dev/{pts,input,net,usb}
    mkdir -p ${DESTDIR}/usr/{bin,include,local,lib,share}
    mkdir -p ${DESTDIR}/var/{cache,lib,local,log,run,spool}
    chmod 1777 ${DESTDIR}/tmp
}

# Complilation du noyau
build_kernel()
{
    cd ${SOURCEDIR}
    wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-${KERNEL}.tar.xz
    tar -xf linux-${KERNEL}.tar.xz
    cd linux-${KERNEL}
    make mrproper
    make ARCH=${ARCH} defconfig
    make ARCH=${ARCH} bzImage -j 2
    make ARCH=${ARCH} modules -j 2
    make INSTALL_MOD_PATH=${DESTDIR} modules_install
    cp arch/x86/boot/bzImage ${ISODIR}/bzImage
}

# Compilation de busybox
build_busybox()
{
    cd ${SOURCEDIR}
    wget http://busybox.net/downloads/busybox-${BUSYBOX}.tar.bz2
    tar -xf busybox-${BUSYBOX}.tar.bz2
    cd busybox-${BUSYBOX}
    cp ${BASEDIR}/packages/busybox/config .config
    make -j 2
    make CONFIG_PREFIX=${DESTDIR} install
    cp examples/bootfloppy/mkdevs.sh ${DESTDIR}/bin
    cd ${DESTDIR}
    chmod 4755 bin/busybox
    rm linuxrc
    ln -sf bin/busybox init
    bin/mkdevs.sh ${DESTDIR}/dev
    
}

# Complilation de musl (libc)
build_musl()
{
    cd ${SOURCEDIR}
    wget https://musl-libc.org/releases/musl-${MUSL}.tar.gz
    tar -xf musl-${MUSL}.tar.gz
    cd musl-${MUSL}
    ./configure --prefix=/ \
    --syslibdir=/lib \
    --enable-optimize=size
    make -j 2
    make DESTDIR=${DESTDIR} install
}

# Syslinux va permettre de rendre notre ISO bootable
build_syslinux()
{
    cd ${SOURCEDIR}
    wget https://mirrors.edge.kernel.org/pub/linux/utils/boot/syslinux/syslinux-${SYSLINUX}.tar.xz
    tar -xf syslinux-${SYSLINUX}.tar.xz
    cd syslinux-${SYSLINUX}
    cp bios/core/isolinux.bin ${ISODIR}
    cp bios/com32/elflink/ldlinux/ldlinux.c32 ${ISODIR}
    cp bios/com32/libutil/libutil.c32 ${ISODIR}
    cp bios/com32/menu/menu.c32 ${ISODIR}
    cd ${ISODIR}
    cp ${BASEDIR}/packages/syslinux/isolinux.cfg .
}

# Remplissage du répertoire etc avec les fichiers fondamentaux
build_etc()
{
    cd ${DESTDIR}
    cp -R ${BASEDIR}/packages/koryoc_base/* .
    chmod +x etc/rc.boot
    chmod +x etc/rc.shutdown
}

# Création initramfs
create_initramfs()
{
    cd ${DESTDIR}
    find . -print | cpio -o -H newc | gzip -9 > ${ISODIR}/rootfs.gz
}

# Création de l'ISO
create_iso()
{
    cd ${ISODIR}
    genisoimage -J -R -o koryoc.iso -b isolinux.bin -c boot.cat -input-charset UTF-8 -no-emul-boot -boot-load-size 4 -boot-info-table ${ISODIR}
}
# Exécution de toutes les fonctions précédemment écrites
prepare_source
construction_arborescence
build_kernel
build_busybox
build_musl
build_syslinux
build_etc
create_initramfs
create_iso

exit 0

