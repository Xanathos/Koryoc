# Koryoc Linux distribution

Distribution Linux minimale incluant musl et BusyBox.
Système de construction simple et clair vous permettant de construire votre système embarqué aux petits oignons.

# Pourquoi réinventer la roue ?

Parce que c'est fun !
Absolument aucune ambition dans ce projet, si ce n'est de poursuivre ma compréhension d'un système d'exploitation.
Le but est aussi de documenter un maximum le process de création d'une ISO bootable pour que ce soit compréhensible par tous.

Par contre, si vous souhaitez un système stable sur lequel vous appuyer pour de la production, passez votre chemin !