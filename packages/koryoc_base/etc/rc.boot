#!/bin/sh

dmesg -n 1

mount -t proc -o nosuid,noexec,nodev /proc /proc
mount -t sysfs -o nosuid,noexec,nodev  /sys /sys
mount -t devtmpfs /dev /dev
mount -t devpts devpts /dev/pts
mount -t tmpfs -o nosuid /tmp /tmp

echo /sbin/mdev > /proc/sys/kernel/hotplug
mdev -s

export HOSTNAME=KORYOC

mount -o remount,ro /
fsck -A -T -C -p
mount -o remount,rw /

dmesg >/var/log/dmesg.log
